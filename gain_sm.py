# Work: Read SM file and parse into XX readings on each field and swath.
# SM file has data on a single laser.
#
# XX readings are timing counts from Vsync to SOL and Vsync to 1st DM.

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import sys
import csv

version = 1.0
# 1/9/2018

# ==============================================================
# ==============================================================

if __name__ == "__main__":
    
    #
    #
    # get files.txt that holds list of file names.
    file1ist = 'files.txt'    # input file list
    path = 'SelfMap/UV_3_11/'
    le = 11
    art = 3
            
    LST0 = []
    with open(file1ist, newline='') as myFile:  
        reader = csv.reader(myFile)
        for row in reader:
            LST0.append(row)
    
    file_df = pd.DataFrame(LST0)
    Nlist = len(file_df)
    
    dat= {'laser': [0], 'field': [0], 'swath': [0], 'sol': [0], 'DM': [0], 'Art': [0], 'LE':[0]}
    table_df = pd.DataFrame(data= dat)
    
    
    for k in range(Nlist):
    
        # get file
        file1 = file_df.loc[k]    # input data
        
        # get laser number from filename
        start= '_L'
        end= '.txt'
        laser = int((file1[0].split(start))[1].split(end)[0])
        
        # get Artix
        start= 'UV_'
        s= (file1[0].split(start))[1]
        Art= int(s[0])
        
        # get LE (row seqence)
        u= s.split("_")
        LE= int(u[1])

        # load file and retrieve data      
        LST1 = []
        with open(path+file1[0], newline='') as myFile:  
            reader = csv.reader(myFile)
            for row in reader:
                LST1.append(row)       
        # convert list to float values
        data = []
        for k in range(len(LST1)):
            yl= LST1[k]
            data.append(float(yl[0]))      
            
        # extract features for single laser
        # at each index read three count values 0: Vsync, Sol, (Sol+UV0)
        # Assumed sequence is swath 0 field 0, 1, 2, swath 1 field 0, 1, 2, ...
           
        index = [0, 27, 54, 81, 108, 135, 162, 189, 216, 243, 270, 297, 
                 324, 351, 378, 405, 432	, 459, 486, 513, 540, 567	, 594, 621]
           
        pntr = 0
        for idx in index:
            sol= data[idx+1]
            DM= data[idx+2] - data[idx+1]
            swath = pntr % 8
            if (pntr < 8):
                field = 0
            if ((pntr > 7) & (pntr < 16)):
                field = 1
            if ((pntr>15)):
                field = 2
                                   
            row= {'laser': [laser], 'field': [field], 'swath': [swath], 'sol': [sol], 'DM': [DM], 'Art': [Art], 'LE':[LE]}
            df1= pd.DataFrame(data= row)
                   
            table_df= table_df.append(df1, ignore_index= True)
            pntr = pntr + 1
            
    # drop initial row that has all zeros
    table_df = table_df.drop(table_df.index[0])   
        
    # plot summary tables
    
    # art = 1, SET AT LINE 23
    # le= 1, SET AT LINE 23
    field= 0

    df = table_df[(table_df['Art']== art) & (table_df['LE']==le) & (table_df['field']==field)]
    
      
    sw0 = df[df['swath']==0]['sol'].values
    La0 = df[df['swath']==0]['laser'].values
    sw1 = df[df['swath']==1]['sol'].values
    La1 = df[df['swath']==0]['laser'].values
    sw2 = df[df['swath']==2]['sol'].values
    La2 = df[df['swath']==0]['laser'].values
    sw3 = df[df['swath']==3]['sol'].values
    La3 = df[df['swath']==0]['laser'].values
    sw4 = df[df['swath']==4]['sol'].values
    La4 = df[df['swath']==0]['laser'].values
    sw5 = df[df['swath']==5]['sol'].values
    La5 = df[df['swath']==0]['laser'].values
    sw6 = df[df['swath']==6]['sol'].values
    La6 = df[df['swath']==0]['laser'].values
    sw7 = df[df['swath']==7]['sol'].values
    La7 = df[df['swath']==0]['laser'].values
     
    plt.figure(1)
    plt.title('sol and DM by swath versus laser')
    plt.axis([0, 20, 4200, 6250])
    plt.plot(La0, sw0, '.', La1, sw1, '.', La2, sw2, '.', La3, sw3, '.', 
             La4, sw4, '.', La5, sw5, '.', La6, sw6, '.', La7, sw7, '.')
    
    swaths = df['DM'].values
    plt.axis([0, 20, 4200, 6250])
    plt.plot(df['laser'].values, swaths,'.')
    plt.xlabel('laser')
    
    plt.show()
    plt.close(1)
    
    
    # compute a normalization using laser 9, field 0, swath 3 as the reference.
    
    ref_sol= df[(df['swath']==3) & (df['laser']==9)]['sol'].values
    
    # plot normalized sol count
    rf = ref_sol
    
    plt.figure(2)
    plt.title('sol gain by swath versus laser')
    plt.axis([0, 20, 0.6, 1.2])
    plt.xlabel('laser')
    plt.plot(La0, sw0/rf, '.', La1, sw1/rf, '.', La2, sw2/rf, '.', La3, sw3/rf, '.', 
             La4, sw4/rf, '.', La5, sw5/rf, '.', La6, sw6/rf, '.', La7, sw7/rf, '.')
    
    plt.show()
    plt.close(2)

    # combined gain correction with default gain   
    default_gn = 0.630   
    
    table_df.sort_values(['field', 'swath', 'laser'], ascending=[True, True, True], inplace=True)
    
    sol_raw = table_df['sol'].values
    gain = default_gn * (sol_raw/ref_sol)
    
    
    plt.figure(3)
    
    plt.title('normalized gain table')
    plt.plot(gain)
    plt.axis([0,500, 0.5, 0.8])
    plt.xlabel('table index')
    
    plt.show()
    plt.close(3)
    
    # output file
        # output file
    OP = []
    for k in range(len(gain)):
        OP.append(str(float(gain[k])))
        OP.append('\n')
    
    file3 = 'gain_norm.csv' 
    opFile = open(file3, 'w+')
    opFile.writelines(OP)
    opFile.close()	
    
    
        
        
        
    
    

    
    
    
    
    