# processEOL.py - process EOL data files
# btremaine 
# process data for full Wall

verDate = 'Oct 31, 2017'
ver = 'V13.2'

import time
tstart = time.time()

import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import operator
import scipy.signal as sig
import logging

# read in file with list of LE data per row 
def read_LE_file(path, file):
    error = 'OK'
    LINE_SZ = 2064 # expected line length
    Lst1 =[]
    N= 0
    os.chdir(path)             # generally c:\IP3Share
    for line in open(file):
        N = N + 1
        r = line.split('\t')
        if len(r)==LINE_SZ:
          Lst1.append(r)
        else:
          error = 'NOK'
          logging.error("Bad line size") 
    HEADER = []
    Lst = []
    X = []
    if error == 'OK':
       Lst = [i for i in Lst1 if i != '']    
       sep = 'IRDAC'  # lastd header
       # parse line into header & data
       HEADER=[]
       X=[]
       first= Lst[0]
       ns = 1 + first.index(sep)
       HEADER = first[0:ns] 
       del Lst[0]
       x = first[ns:]
       X = list(map(int,x))
    
    return Lst, HEADER, X, error
    
def read_file_list(path, file):
    list1 =[]
    os.chdir(path)             # generally c:\IP3Share
    for line in open(file):
        r = line.rstrip('\n')
        list1.append(r)
    #
    file_names = [x for x in list1 if x != '']
    Nf = len(file_names)  

    return file_names, Nf

def parse_line(Lst, nh):	
    # parse line into cfg & data
    DATA=[]
    CFG=[]
    first = Lst
    CFG = first[0:nh] 
    data = first[nh:]
    DATA = list(map(int, data))

    return DATA, CFG

def getThresh(dat, w1, w2):
	# find threshold
	
	LEV = 0.20
	index, pkmax = max(enumerate(list(dat[w1:w2])), key=operator.itemgetter(1))
	index = index + w1
	thr = pkmax * LEV
	return index, thr, pkmax

def filter_sig(dat):
    # Gaussian matched filter
	# can use fixed coefficients in software
   Nc = 6;
   Ntau = 2
   xf = np.asarray(range(-Nc,Nc+1))
   af= np.exp((-((xf/Ntau)**2)))
   gn= 1.0/sum(af)
   y = gn*np.convolve(np.asarray(dat),af,'same')
	
   if(SCALE==6):
     #interpolate 6x
     x = (1/6) * (np.array(range(0,6*len(dat)-1))-1)
   else:
     #interpolate 3x
     x = (1/3) * (np.array(range(0,3*len(dat)-1))-1) 
   xp = np.array(range(0,len(y)))
   yi = np.interp(x,xp,y)
	
   if(SCALE!=6):
     # decimate 2x
     yi = sig.decimate(yi, 2, 8, 'iir', 0 , 'None')
	
   return yi
	
def dcRestore(dat):
	# find threshold
	ydc = np.zeros(len(dat))
	
	for i in range(len(dat)-1):
	   slope = (dat[i]-ydc[i])
	   if slope> 0 :
	      gain = 0.5
	   else:
	      gain = -10.0	  
	   ydc[i+1] = ydc[i] + gain
	dc = np.mean(ydc)
	  
	return dc

def findLocalPeaks(y,R,thr):
	# find peak in y in range R140/912
	# R: range of data
	# thr: threshold in ADC counts
	
    if not thr:
      L= np.zeros(0)
      return L.astype(int)

    numPeaks = 9
    L = np.zeros(numPeaks)
    trackedIndex = 0
    vmax = 0.
	
    j = 0
    for k in R:
      if (y[k] > vmax) & (y[k]>thr) & (k!=R[0]):
        trackedIndex = k
        vmax = y[k]
      if( (vmax>thr) & (y[k]< 0.50*thr)): # was 0.80*thrL then 0.25*vmax
        L[j] = trackedIndex
        #print(j,k,thr,vmax)
        vmax = 0
        j= j+1
      if j> numPeaks-1:
        break

    return L.astype(int)

def getPeaks(dat, win1, win2):
	# using filtered data
    # define windows to search (post interpolation)
    # error if win1, win2 too large
    Error= 'OK'
    if (win1 > len(dat)) | (win2 > len(dat)):
       L = [0]
       Error= 'NOK'
    else:
      index, thr, pkmax = getThresh(dat,win1,win2)
      #print(['thr',thr])
      #
      # choose peaks
      if( pkmax > 200):
            R= range(win1,win2)
            L= findLocalPeaks(dat,R,thr)
            L= removeZeros(L)
            if L[0]:
                L= removeReflec(dat,L)
            else:
                Error= 'NOK'
      else:
        L= np.zeros(1)
        Error = 'NOK'
        pkmax= 0
        
    return L, Error, pkmax
	
def removeZeros(L):
	if L[0] == 0:
	  L = np.zeros(1)
	  L = L.astype(int)
	else:
	  # width
	  Llist = list(L)
	  try:
	    while 0 in Llist: Llist.remove(0)
	  except ValueError:
	    pass
	  L = np.asarray(Llist)
	return L
	
def checkSpread(L, y, arg, nearest):
	#
    Error = 'OK'
    L = list(L)
    if (not L) | (len(L)<2):
      Error= 'NOK'
      return np.zeros(1), Error
    #
    SpreadMax = 221 * SCALE
    SpreadMin = 206 * SCALE
    len1 = len(L)
    #
    count = []
    for i in range(len1-1):
      last = L[i] + SpreadMax
      # find index j>i for value <= last
      j = i
      k = True
      while ( k & (L[j] <= last) ):
        j= j+1
        if j >= len1:
          k= False 
          break
      # count members in range i to j
      count.append(len(range(i,j)))
    # check SpreadMin requirement
    countOk = count;
    for i in range(len(count)):
      if L[i+count[i]-1] - L[i] < SpreadMin :
        countOk[i]= 0      
    # check for empty set
    Ix, err = max(enumerate(list(countOk)), key=operator.itemgetter(1))
    if countOk[Ix] ==0:
      Error= 'NOK'
      return np.zeros(1), Error  
    if(arg==False):
      # for IR choose range with largest # of members
      # apply heuristic - if more than two qualified, choose one with 1st/last closest to unity
      ratio = [];
      for i in range(len(countOk)):
        if countOk[i]!=0:
          a = y[L[i+count[i]-1]]
          b = y[L[i]]
          if a>b :
            ratio.append(b/a)
          else:
            ratio.append(a/b)
        else:
          ratio.append(0)   
      #print(ratio) 
      #print(count)
      index, err = max(enumerate(list(ratio)), key=operator.itemgetter(1))
      L= L[index:index+countOk[index]]
    else:
      # for UV use qualified window closest to most recent value,Tnearest
      #
      disMax = 10000
      j = 0
      for i in range(len(countOk)):
        if countOk[i]!=0:
          dis = min(disMax,abs(nearest-L[i]))
          if dis<disMax:
            disMax = dis
            j = i
      #print([j, dis])
      L= L[j:(countOk[j]+j)]
    return np.asarray(L), Error

def VaMetric(P,dat):
	error = 'NOK'
	if len(P) < 3:
	  error = 'NOK'
	  metric = 0
	  L= 0
	else:
	  #
	  # replace index with getting database value
	  index, value = max(enumerate(list(dat[P])), key=operator.itemgetter(1))
	  error = 'OK' 
	  width = max(P)-min(P)
	  # maxima based metric: #################
	  # L = index
	  # metric = (P[L] - min(P))/width
	  #
	  # axis-of-symm based metric ############
	  side = 20
	  L, err = AxisSymm(dat, P[index], side)
	  metric = (L-min(P))/width
	  #
	  metric = int(metric*(2**16) - 2**1)

	return metric, error, L

def removeReflec(dat,L):
	# checks for wide base & rejects
    Li= list(L)
    Lcpy= list(L)
    wid1= []
    wid2= []
    for j in Li:
      center = j
      peak = dat[center]
	  # find left & right low % amplitude
      left = center
      while (dat[left] > 0.25*peak) & (left>0):
        left = left -1
      right = center
      while (dat[right] > 0.25*peak) & (right<(2048 * SCALE)):
        right = right +1
      # find left & right hi % amplitude
      leftHi = center
      while (dat[leftHi] > 0.75*peak) & (leftHi>0):
        leftHi = leftHi -1
      rightHi = center
      while (dat[rightHi] > 0.75*peak) & (rightHi<(2048 * SCALE)):
        rightHi = rightHi +1
	  # apply restriction
      w1 = rightHi - leftHi # width at 75%
      w2 = right - left     # width at 25%
      wid1.append(w1)
      wid2.append(w2)
      #
    for k in range(len(Li)):
      logging.debug([k,'width',Li[k], wid1[k], wid2[k]])
      #print([k,'width',Li[k], wid1[k], wid2[k]])
      if ( (wid2[k]>70.0*SCALE) or (wid1[k]>26.7*SCALE) or (wid2[k]<=wid1[k]) ):
         Lcpy.remove(Li[k])
    L= np.array(Lcpy)
    return L

def AxisSymm(dat, Lpeak, side):
	error = np.zeros(2*side+1)
	for j in range(0,2*side+1):
	  Lopt = Lpeak + j - side
	  w1 = range(Lopt-side, Lopt)
	  w2 = range(Lopt, Lopt+side)
	  error[j] = -(np.sum(dat[w2]-dat[w1]))**2

	index, err = max(enumerate(list(error)), key=operator.itemgetter(1))
	L = Lpeak + index - side
	L
	return L, err
    
def getUvWindow(t1,tlead):
    error = 'NOK'
    W3 = 0
    W4 = 0
    if (t1 >0) & (tlead>0) & ((t1+tlead)>500):
      error = 'OK'
      W3 = int(t1 + tlead - 1000)
      W4 = int(W3 + 4000)

    return W3, W4, error

def interpDelta(jt1,jt2,dat):
    # interpolate to finer index
    p1 = jt1
    p2 = jt2
    if( (jt1>1) & (jt2>1)):
      x1= [jt1-1, jt1, jt1+1]
      y1= [dat[x1[0]], dat[x1[1]], dat[x1[2]]]
      x2= [jt2-1, jt2, jt2+1]
      y2= [dat[x2[0]], dat[x2[1]], dat[x2[2]]]
      z1= np.polyfit(x1,y1,2)
      z2= np.polyfit(x2,y2,2)
      #
      p1= -z1[1]/(2.0*z1[0])
      p2= -z2[1]/(2.0*z2[0])
    
    return p1, p2
    
def setScale():
    # use 1.5 or 6.0
    global SCALE
    SCALE = 6.0 
                       
def Normalize(LE, tfarray):
    LEc = LE
    Vmeas = 0
    for k in range(len(LE)):
        Vmeas = Vmeas + tfarray[k][1]
    Vmeas = Vmeas / len(LE)
    for j in range(len(LE)):  
        new = tfarray[j][2] * (1 + j*ALPHA*(1 - VREF/Vmeas) )
        if DB2 == False:
            new = new * 1.50/SCALE
        LEc[j][3] = str(int(new))
    
    return LEn, Vmeas

def formatOP(LE):
    # format for file output
    OP = []
    for k in range(len(LE)):
        OP.append(LE[k][0]+'\t'+LE[k][1]+'\t'+LE[k][2]+'\t'+LE[k][3]+'\t' + 
           LE[k][4]+'\t'+LE[k][5]+'\t'+LE[k][6]+'\n')
        
    return OP
        
    
# ===================================================
# ===================================================
if __name__ == '__main__':
    
    # logging set-up
    logging.basicConfig(filename='processEOL.log',filemode='w',level=logging.DEBUG)
    logging.info("Program started")   
    
    if len(sys.argv) > 2:
        path = sys.argv[1]
        file = sys.argv[2]
        
        flag = []
        try:
            flag = sys.argv[3]
        except Exception:
            pass
         
    # read list of Wall files, typ files.txt
    file_names, Nf = read_file_list(path, file)
      
    global DB2
    global SCALE
    global VREF
    global ALPHA
    ALPHA = -0.030
    VREF = 1277.0
    DB2 = True   # set true for DB2.0 with 600MHz clock
    
    setScale()
  
    for k in range(Nf):
        # read LE file   
        Lst, HEADER, X, error = read_LE_file(path, file_names[k])	
        # loop through entries (lasers) loaded from file
        OP = [] # output file list
        nh = len(HEADER)
        N= len(Lst)
        if N>0:
           LE = []
           tfarray = np.zeros([N,3])
           for i in range(N):
               [data, cfg] = parse_line(Lst[i],nh)
            
               status = 0
               y = filter_sig(data)
               # remove DC offset
               ydc = dcRestore(y)
               y = y - ydc

               # process IR
               W1 = 0
               W2 = int(600 * SCALE)
               L1, error, IRpk = getPeaks(y, W1, W2)
               if error != 'OK':
                 status = 1
          
               if status == 0:
                   P1, error = checkSpread(L1, y, False, 200)
                   t1 = P1[0]	
                   t1a = P1[0]
                   t1b = P1[-1]
               else:
                   t1=0
                   t2=0
                   W3= W2
                   W4= W1
                   P1= np.array([0])

               if error != 'OK':
                 status = status | (1<<1)

               # process UV   # tlead = float(cfg[14])
               tlead = int(cfg[14])

               tguess = t1+tlead
               W3, W4, error = getUvWindow(t1,tlead)
               
               #print([t1, tlead, W3, W4, error])
            
               if error == 'OK':
                 L2, error, UVpk = getPeaks(y, W3, W4)      

               if error == 'NOK':
                 L2 = 0
                 UVpk = 0
                 status = status | (1<<2)  
              
               t2_minus_t1 = 0
               if error == 'OK':              
                   P2, error = checkSpread(L2, y, False,tguess)
                   t2 = P2[0]  
                   t2a= P2[0]
                   t2b= P2[-1]
                   
                   # preliminary before normalization
                   # interpolation bug
                   #tf1a,tf2a = interpDelta(t1a, t2a, y)
                   #tf1b,tf2b = interpDelta(t1b, t2b, y)              
                   tf1a = t1a
                   tf2a = t2a
                   tf1b = t1b
                   tf2b = t2b                   
                   
                   
                   t2_minus_t1 = (tf2a+tf2b-tf1a-tf1b)/2
                   
                   span1 = tf1b - tf1a
                   
                   tfarray[i][0]= int(i)
                   tfarray[i][1]= span1
                   tfarray[i][2]= t2_minus_t1 # before normalization
                
               if error == 'NOK':
                   t2= t1
                   status = status | (1<<3)


               if DB2 != True:
                  # short-term change for 1.5X code
                  t2_minus_t1 = round(t2_minus_t1/4)              
            
               # LE output is metric, t1 and t2, or (t2-t1)
               LE.append([str(cfg[0]), str(cfg[2]), str(int(t1/4)), \
                          str(int(t2_minus_t1)), str(status), \
                          str(int(IRpk)), str(int(UVpk))])

               # plot utilities for debugging
               if(flag):
                # ----------------------------------------------
                # Display only, not needed in real-time code
                # ----------------------------------------------
                   print(cfg[2],int(t1/4),t2_minus_t1,status)

                   st = ['laser',cfg[2],cfg[0],cfg[9],error]
                   plt.plot(y[0:3072 * 4])
                
                   try: varChk = [L1,L2,P1,P2]
                   except NameError:
                     print('none existent variables - skip plots')
                   else: 
                     plt.plot(L1, y[L1.astype(int)],'rx')
                     plt.plot(L2, y[L2.astype(int)],'bx')
                     plt.plot(P1, y[P1.astype(int)]+25,'ro')
                     plt.plot(P2, y[P2.astype(int)]+25,'bo')
                     #VA plt.plot(L, y[L]+50,'go')
                     plt.plot([W3,W3],[0,500],':r')
                     plt.plot([W4,W4],[0,500],':r')
                     plt.title(st)
                     h1= 600
                     h2= int(h1+50)
                     plt.text(50,h1,P1)
                     plt.text(50,h2,P2)
                  
                   plt.show()
               # ---------------------------------------------------
        
           # end LE loop
           
           # Normalize using Vmeas =: average tf1b-tf1a across all measured lasers
           # Scale t2_minus_t1 using Vmeas and Vref and gain alpha 
           
           LEn = LE
           Vmeas = VREF
           if (error == 'OK'):
             LEn, Vmeas = Normalize(LE, tfarray)
           else:
             LEn = LE
             
           OP = formatOP(LEn)
          
           # update current output file
           OPVer = ver + '\n'
           OPHeader = "Tile\tLaser#\tt1\tt2-t1\tstatus" + '\tIRpk' + '\tUVpk' + '\t' + '\n'
           file1 = 'opFile_' + str(cfg[0]) + '.txt' 
           opFile = open(file1, 'w+')
           opFile.writelines(OPVer)
           opFile.writelines(OPHeader)
           opFile.writelines(OP)
           opFile.close()	
                

tfin = time.time() - tstart
logging.info(tfin)



